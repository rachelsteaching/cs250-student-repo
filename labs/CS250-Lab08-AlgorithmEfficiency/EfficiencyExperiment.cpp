#include "EfficiencyExperiment.hpp"

#include <iostream>
using namespace std;

/*******************************/
/** IMPLEMENT THESE FUNCTIONS **/
/*******************************/

/**
This class contains a vector<int> m_array. Search this m_array.

@param      int     findMe          The # to find in the array
@param      bool    withOutput      Whether or not to display text as you're searching.
@return     int                     The index where the item was found, or -1 if not found.
*/
int EfficiencyExperiment::LinearSearch( int findMe, bool withOutput )
{
	for (int i = 0; i < m_array.size(); i++)
	{
		//if (withOutput) { cout << "Searching item " << i << "... " << endl; }
		if (m_array[i] == findMe)
		{
			return i;
		}
	}
	return -1;
}

/**
This class contains a vector<int> m_array. Search this m_array.

@param      int     findMe          The # to find in the array
@param      bool    withOutput      Whether or not to display text as you're searching.
@return     int                     The index where the item was found, or -1 if not found.
*/
int EfficiencyExperiment::BinarySearch( int findMe, bool withOutput )
{
    return -1; // temporary
}

/**
This class contains a vector<int> m_array. Search this m_array.

Come up with your own search method and implement it here.

@param      int     findMe          The # to find in the array
@param      bool    withOutput      Whether or not to display text as you're searching.
@return     int                     The index where the item was found, or -1 if not found.
*/
int EfficiencyExperiment::StudentSearch( int findMe, bool withOutput )
{
    return -1; // temporary
}


/**
n:              1   2   3   4   5   6   7   8   9   10   ...
n-th term:      1   1   2   3   5   8   13  21  34  55  ...

The Fibonacci sequence at a[n] is going to be a[n-1] + a[n-2],
or, the value of the previous two numbers in the sequence summed together.

@param      int     n               The "position" of the number to generate.
@return     int                     The n-th value of the Fibonacci sequence
*/
int EfficiencyExperiment::Fibonacci_Rec(unsigned int n )
{
    return -1; // temporary
}

/**
@param      int     n               The "position" of the number to generate.
@return     int                     The n-th value of the Fibonacci sequence
*/
int EfficiencyExperiment::Fibonacci_Iter( unsigned int n )
{
    return -1; // temporary
}

/*********************************/
/** DON'T EDIT THESE            **/
/*********************************/

void EfficiencyExperiment::SetupVector( int arraySize, bool sorted )
{
    if ( m_array.size() > 0 ) { m_array.clear(); }

    for ( int i = 0; i < arraySize; i++ )
    {
        if ( sorted )
        {
            m_array.push_back( i+5 );
            m_listSorted = true;
        }
        else
        {
            // Random #s
            m_array.push_back( rand() % ( arraySize * 10 + 1 ) );
            m_listSorted = false;
        }
    }
}

int EfficiencyExperiment::Size()
{
    return m_array.size();
}
