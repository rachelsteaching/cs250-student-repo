#include <iostream>
#include <queue>
using namespace std;

#include "FactorialComputer.hpp"

int main()
{
    FactorialComputer fc;
    fc.Run();

    cout << endl << "DONE" << endl;

    cin.ignore();
    cin.get();

    return 0;
}
