#include <iostream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "UTILITIES/Logger.hpp"

#include "FactoryProgram.hpp"
#include "Tester.hpp"

int main()
{
    Logger::Setup( false );
    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "PROJECT 3/4 MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( { "Run Linked List tester", "Run program", "Quit" } );

        switch( choice )
        {
            case 1:
            {
                Tester tester;
                tester.Start();
                Menu::Pause();
            }
            break;

            case 2:
            {
                FactoryProgram fp;
                fp.Run();
                cout << "DONE" << endl;
                Menu::Pause();
            }
            break;

            case 3:
            done = true;
            break;
        }
    }
    Logger::Cleanup();

    return 0;
}
