#ifndef _TESTER_HPP
#define _TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "CUTEST/TesterBase.hpp"
#include "CUTEST/Menu.hpp"
#include "CUTEST/StringUtil.hpp"

#include "DATASTRUCTURES/LinkedList.hpp"

class Tester : public TesterBase
{
public:
	Tester()
	{
		AddTest(TestListItem("Test_NodeConstructor",         bind(&Tester::Test_NodeConstructor, this)));
		AddTest(TestListItem("Test_Constructor",             bind(&Tester::Test_Constructor, this)));
		AddTest(TestListItem("Test_Clear",                   bind(&Tester::Test_Clear, this)));
		AddTest(TestListItem("Test_PushFront",               bind(&Tester::Test_PushFront, this)));
		AddTest(TestListItem("Test_PushBack",                bind(&Tester::Test_PushBack, this)));
		AddTest(TestListItem("Test_PopFront",                bind(&Tester::Test_PopFront, this)));
		AddTest(TestListItem("Test_PopBack",                 bind(&Tester::Test_PopBack, this)));
		AddTest(TestListItem("Test_GetFront",                bind(&Tester::Test_GetFront, this)));
		AddTest(TestListItem("Test_GetBack",                 bind(&Tester::Test_GetBack, this)));
		AddTest(TestListItem("Test_SubscriptOperator",       bind(&Tester::Test_SubscriptOperator, this)));
		AddTest(TestListItem("Test_IsEmpty",                 bind(&Tester::Test_IsEmpty, this)));
		AddTest(TestListItem("Test_Size",                    bind(&Tester::Test_Size, this)));
	}

	virtual ~Tester() { }

private:
	int Test_NodeConstructor();
	int Test_Constructor();
	int Test_Clear();
	int Test_PushFront();
	int Test_PushBack();
	int Test_PopFront();
	int Test_PopBack();
	int Test_GetFront();
	int Test_GetBack();
	int Test_SubscriptOperator();
	int Test_IsEmpty();
	int Test_Size();
};

int Tester::Test_NodeConstructor()
{
    StartTestSet( "Test_NodeConstructor", { "" } );
    ostringstream oss;

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			Node<int> test;
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When a new node is created, m_ptrPrev should be nullptr." ); {
        Set_Comments( "" );

        Node<int> node;

        Set_ExpectedOutput  ( "Node's m_ptrPrev is nullptr" );
        if ( node.m_ptrPrev == nullptr )
        {
            Set_ActualOutput    ( "Node's m_ptrPrev is nullptr" );
            TestPass();
        }
        else
        {
            oss << node.m_ptrPrev;
            Set_ActualOutput    ( "Node's m_ptrPrev", oss.str() );
            TestFail();
        }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When a new node is created, m_ptrNext should be nullptr." ); {
        Set_Comments( "" );

        Node<int> node;

        Set_ExpectedOutput  ( "Node's m_ptrNext is nullptr" );
        if ( node.m_ptrPrev == nullptr )
        {
            Set_ActualOutput    ( "Node's m_ptrNext is nullptr" );
            TestPass();
        }
        else
        {
            oss << node.m_ptrNext;
            Set_ActualOutput    ( "Node's m_ptrNext", oss.str() );
            TestFail();
        }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_Constructor()
{
	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.Clear();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}
    StartTestSet( "Test_Constructor", { "" } );
    ostringstream oss;

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When a LinkedList is created, m_ptrFirst should be nullptr" ); {
        Set_Comments( "" );

        LinkedList<int> ll;

        Set_ExpectedOutput  ( "m_ptrFirst is nullptr" );
        if ( ll.m_ptrFirst == nullptr )
        {
            Set_ActualOutput    ( "m_ptrFirst is nullptr" );
            TestPass();
        }
        else
        {
            oss << ll.m_ptrFirst;
            Set_ActualOutput    ( "m_ptrFirst", oss.str() );
            TestFail();
        }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When a LinkedList is created, m_ptrLast should be nullptr" ); {
        Set_Comments( "" );

        LinkedList<int> ll;

        Set_ExpectedOutput  ( "m_ptrLast is nullptr" );
        if ( ll.m_ptrLast == nullptr )
        {
            Set_ActualOutput    ( "m_ptrLast is nullptr" );
            TestPass();
        }
        else
        {
            oss << ll.m_ptrLast;
            Set_ActualOutput    ( "m_ptrLast", oss.str() );
            TestFail();
        }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When a LinkedList is created, m_itemCount should be 0" ); {
        Set_Comments( "" );

        LinkedList<int> ll;

        int expectedOutput = 0;
        int actualOutput = ll.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount", expectedOutput );
        Set_ActualOutput    ( "m_itemCount", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    FinishTestSet();
    return TestResult();
}

int Tester::Test_Clear()
{
    StartTestSet( "Test_Clear", { "PopFront", "PushBack", "Size" } );
    ostringstream oss;

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(1);
			test.PopFront();
			test.Clear();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "Does Size return 0 after Clear?" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "A" );
        ll.PushBack( "B" );
        ll.PushBack( "C" );

        ll.Clear();

        int expectedOutput = 0;
        int actualOutput = ll.Size();

        Set_ExpectedOutput  ( "Size()", expectedOutput );
        Set_ActualOutput    ( "Size()", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "is first and last pointer pointing to nullptr after Clear?" ); {
        Set_Comments( "" );

        LinkedList<string> ll;
        ll.PushBack( "A" );
        ll.PushBack( "B" );
        ll.PushBack( "C" );

        ll.Clear();

        Node<string>* ptrFirst_expectedOutput = nullptr;
        Node<string>* ptrFirst_actualOutput = ll.m_ptrFirst;
        Node<string>* ptrLast_expectedOutput = nullptr;
        Node<string>* ptrLast_actualOutput = ll.m_ptrLast;

        oss << ptrFirst_expectedOutput;
        Set_ExpectedOutput  ( "List's m_ptrFirst", oss.str() );
        oss << ptrFirst_actualOutput;
        Set_ActualOutput    ( "List's m_ptrFirst", oss.str() );

        oss << ptrLast_expectedOutput;
        Set_ExpectedOutput  ( "List's m_ptrLast", oss.str() );
        oss << ptrLast_actualOutput;
        Set_ActualOutput    ( "List's m_ptrLast", oss.str() );

        if ( ptrFirst_expectedOutput != ptrFirst_actualOutput )     { TestFail(); }
        else if ( ptrLast_expectedOutput != ptrLast_actualOutput )  { TestFail(); }
        else                                                        { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_PushFront()
{
    StartTestSet( "Test_PushFront", { "" } );
    ostringstream oss;

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushFront(2);
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When item is added to empty list, m_ptrFirst and m_ptrLast should point at it." ); {
        Set_Comments( "Can't directly verify addresses; verifying by data in the CustomerData." );

        LinkedList<string> ll;

        ll.PushFront( "cheeseburger" );

        string expectedOutput = "cheeseburger";
        string actualOutput = ll.m_ptrFirst->m_data;

        Set_ExpectedOutput  ( "Front item", expectedOutput );
        Set_ActualOutput    ( "Front item", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When two items are added to an empty list, one should be pointed to by m_ptrFirst, and the other should be pointed to by m_ptrLast" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushFront( "one" );
        ll.PushFront( "two" );

        string expectedOutput_first = "two";
        string actualOutput_first = ll.m_ptrFirst->m_data;
        string expectedOutput_last = "one";
        string actualOutput_last = ll.m_ptrLast->m_data;

        Set_ExpectedOutput  ( "FIRST data", expectedOutput_first );
        Set_ActualOutput    ( "FIRST data", actualOutput_first );
        Set_ExpectedOutput  ( "LAST data", expectedOutput_last );
        Set_ActualOutput    ( "LAST data", actualOutput_last );

        if ( expectedOutput_first != actualOutput_first )    { TestFail(); }
        else if ( expectedOutput_last != actualOutput_last ) { TestFail(); }
        else                                                 { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When two items are added to an empty list, the two items should point at each other." ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushFront( "fries" );
        ll.PushFront( "burger" );

        Node<string>* expectedOutput_first_ptrNext = ll.m_ptrLast;
        Node<string>* actualOutput_first_ptrNext = ll.m_ptrFirst->m_ptrNext;
        Node<string>* expectedOutput_last_ptrPrev = ll.m_ptrFirst;
        Node<string>* actualOutput_last_ptrPrev = ll.m_ptrLast->m_ptrPrev;

        oss << expectedOutput_first_ptrNext;
        Set_ExpectedOutput  ( "FIRST Node's ptrNext", oss.str() );
        oss << actualOutput_first_ptrNext;
        Set_ActualOutput    ( "FIRST Node's ptrNext", oss.str() );

        oss << expectedOutput_last_ptrPrev;
        Set_ExpectedOutput  ( "LAST Node's ptrPrev", oss.str() );
        oss << actualOutput_last_ptrPrev;
        Set_ActualOutput    ( "LAST Node's ptrPrev", oss.str() );

        if ( expectedOutput_first_ptrNext != actualOutput_first_ptrNext )       { TestFail(); }
        else if ( expectedOutput_last_ptrPrev  != actualOutput_last_ptrPrev )   { TestFail(); }
        else                                                                    { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_PushBack()
{
    StartTestSet( "Test_PushBack", { "" } );
    ostringstream oss;

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(2);
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When item is added to empty list, m_ptrFirst and m_ptrLast should point at it." ); {
        Set_Comments( "Can't directly verify addresses; verifying by data in the CustomerData." );

        LinkedList<string> ll;

        ll.PushBack( "pizza" );

        string expectedOutput = "pizza";
        string actualOutput = ll.m_ptrFirst->m_data;

        Set_ExpectedOutput  ( "first data", expectedOutput );
        Set_ActualOutput    ( "first data", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When two items are added to an empty list, one should be pointed to by m_ptrFirst, and the other should be pointed to by m_ptrLast" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "amethyst" );
        ll.PushBack( "pearl" );

        string expectedOutput_first = "amethyst";
        string actualOutput_first = ll.m_ptrFirst->m_data;
        string expectedOutput_last = "pearl";
        string actualOutput_last = ll.m_ptrLast->m_data;

        Set_ExpectedOutput  ( "FIRST data", expectedOutput_first );
        Set_ActualOutput    ( "FIRST data", actualOutput_first );
        Set_ExpectedOutput  ( "LAST data", expectedOutput_last );
        Set_ActualOutput    ( "LAST data", actualOutput_last );

        if ( expectedOutput_first != actualOutput_first )    { TestFail(); }
        else if ( expectedOutput_last != actualOutput_last ) { TestFail(); }
        else                                                 { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "When two items are added to an empty list, the two items should point at each other." ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "apple" );
        ll.PushBack( "banana" );

        Node<string>* expectedOutput_first_ptrNext = ll.m_ptrLast;
        Node<string>* actualOutput_first_ptrNext = ll.m_ptrFirst->m_ptrNext;
        Node<string>* expectedOutput_last_ptrPrev = ll.m_ptrFirst;
        Node<string>* actualOutput_last_ptrPrev = ll.m_ptrLast->m_ptrPrev;

        oss << expectedOutput_first_ptrNext;
        Set_ExpectedOutput  ( "FIRST Node's ptrNext", oss.str() );
        oss << actualOutput_first_ptrNext;
        Set_ActualOutput    ( "FIRST Node's ptrNext", oss.str() );

        oss << expectedOutput_last_ptrPrev;
        Set_ExpectedOutput  ( "LAST Node's ptrPrev", oss.str() );
        oss << actualOutput_last_ptrPrev;
        Set_ActualOutput    ( "LAST Node's ptrPrev", oss.str() );

        if ( expectedOutput_first_ptrNext != actualOutput_first_ptrNext )       { TestFail(); }
        else if ( expectedOutput_last_ptrPrev  != actualOutput_last_ptrPrev )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_PopFront()
{
    StartTestSet( "Test_PopFront", { "PushBack", "GetFront", "GetBack" } );

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(2);
			test.GetFront();
			test.PopFront();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "Popping an item should decrease the item count" ); {
        Set_Comments( "Popping the last item" );

        LinkedList<string> ll;

        ll.PushBack( "saluton" );

        int expectedOutput_prePop = 1;
        int actualOutput_prePop = ll.m_itemCount;

        ll.PopFront();

        int expectedOutput_postPop = 0;
        int actualOutput_postPop = ll.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount, before pop", expectedOutput_prePop );
        Set_ActualOutput    ( "m_itemCount, before pop", actualOutput_prePop );
        Set_ExpectedOutput  ( "m_itemCount, after pop", expectedOutput_postPop );
        Set_ActualOutput    ( "m_itemCount, after pop", actualOutput_postPop );

        if ( expectedOutput_prePop != actualOutput_prePop )         { TestFail(); }
        else if ( expectedOutput_postPop != actualOutput_postPop )  { TestFail(); }
        else                                                        { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "Popping an item should decrease the item count" ); {
        Set_Comments( "Popping an item and still having an item left over" );

        LinkedList<string> ll;

        ll.PushBack( "steven" );
        ll.PushBack( "connie" );

        int expectedOutput_prePop = 2;
        int actualOutput_prePop = ll.m_itemCount;

        ll.PopFront();

        int expectedOutput_postPop = 1;
        int actualOutput_postPop = ll.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount, before pop", expectedOutput_prePop );
        Set_ActualOutput    ( "m_itemCount, before pop", actualOutput_prePop );
        Set_ExpectedOutput  ( "m_itemCount, after pop", expectedOutput_postPop );
        Set_ActualOutput    ( "m_itemCount, after pop", actualOutput_postPop );

        if ( expectedOutput_prePop != actualOutput_prePop )         { TestFail(); }
        else if ( expectedOutput_postPop != actualOutput_postPop )  { TestFail(); }
        else                                                        { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "Pushing Back cd1 then cd2, a PopFront should leave cd2 and cd3." ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "A" );
        ll.PushBack( "B" );
        ll.PushBack( "C" );

        ll.PopFront();

        string front_expectedOutput = "B";
        string front_actualOutput = ll.GetFront();
        string back_expectedOutput = "C";
        string back_actualOutput = ll.GetBack();

        Set_ExpectedOutput  ( "Front-most item", front_expectedOutput );
        Set_ActualOutput    ( "Front-most item", front_actualOutput );
        Set_ExpectedOutput  ( "Back-most item", back_expectedOutput );
        Set_ActualOutput    ( "Back-most item", back_actualOutput );

        if ( front_expectedOutput != front_actualOutput )           { TestFail(); }
        else if ( back_expectedOutput != back_actualOutput )        { TestFail(); }
        else                                                        { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_PopBack()
{
    StartTestSet( "Test_PopBack", { "PushBack", "GetFront", "GetBack" } );

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(2);
			test.GetFront();
			test.PopBack();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "Popping an item should decrease the item count" ); {
        Set_Comments( "Popping the last item" );

        LinkedList<string> ll;

        ll.PushBack( "Guybrush" );

        int expectedOutput_prePop = 1;
        int actualOutput_prePop = ll.m_itemCount;

        ll.PopBack();

        int expectedOutput_postPop = 0;
        int actualOutput_postPop = ll.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount, before pop", expectedOutput_prePop );
        Set_ActualOutput    ( "m_itemCount, before pop", actualOutput_prePop );
        Set_ExpectedOutput  ( "m_itemCount, after pop", expectedOutput_postPop );
        Set_ActualOutput    ( "m_itemCount, after pop", actualOutput_postPop );

        if ( expectedOutput_prePop != actualOutput_prePop )         { TestFail(); }
        else if ( expectedOutput_postPop != actualOutput_postPop )  { TestFail(); }
        else                                                        { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "Popping an item should decrease the item count" ); {
        Set_Comments( "Popping an item and still having an item left over" );

        LinkedList<string> ll;

        ll.PushBack( "Keen" );

        ll.PushBack( "Doom" );

        int expectedOutput_prePop = 2;
        int actualOutput_prePop = ll.m_itemCount;

        ll.PopBack();

        int expectedOutput_postPop = 1;
        int actualOutput_postPop = ll.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount, before pop", expectedOutput_prePop );
        Set_ActualOutput    ( "m_itemCount, before pop", actualOutput_prePop );
        Set_ExpectedOutput  ( "m_itemCount, after pop", expectedOutput_postPop );
        Set_ActualOutput    ( "m_itemCount, after pop", actualOutput_postPop );

        if ( expectedOutput_prePop != actualOutput_prePop )         { TestFail(); }
        else if ( expectedOutput_postPop != actualOutput_postPop )  { TestFail(); }
        else                                                        { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "Pushing Back cd1 then cd2, a PopFront should leave cd2 and cd3." ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "spyro the dragon" );
        ll.PushBack( "ripto's rage" );
        ll.PushBack( "year of the dragon" );

        ll.PopBack();

        string front_expectedOutput = "spyro the dragon";
        string front_actualOutput = ll.GetFront();
        string back_expectedOutput = "ripto's rage";
        string back_actualOutput = ll.GetBack();

        Set_ExpectedOutput  ( "Front-most item", front_expectedOutput );
        Set_ActualOutput    ( "Front-most item", front_actualOutput );
        Set_ExpectedOutput  ( "Back-most item", back_expectedOutput );
        Set_ActualOutput    ( "Back-most item", back_actualOutput );

        if ( front_expectedOutput != front_actualOutput )           { TestFail(); }
        else if ( back_expectedOutput != back_actualOutput )        { TestFail(); }
        else                                                        { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_GetFront()
{
    StartTestSet( "Test_GetFront", { "PushBack" } );

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(2);
			test.GetFront();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "GetFront should throw exception if list is empty" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        bool threwException = false;

        try
        {
            string cd = ll.GetFront();
        }
        catch( ... )
        {
            threwException = true;
        }

        Set_ExpectedOutput  ( "Exception thrown" );
        if ( threwException )   Set_ActualOutput    ( "Exception thrown" );
        else                    Set_ActualOutput    ( "Exception not thrown" );

        if ( threwException == false )   { TestFail(); }
        else                             { TestPass(); }
    } FinishTest();


    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "After inserting several items, GetFront should get the front." ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "secret of monkey island" );
        ll.PushBack( "lechuck's revenge" );
        ll.PushBack( "curse of monkey island" );

        string expectedOutput = "secret of monkey island";
        string actualOutput = ll.GetFront();

        Set_ExpectedOutput  ( "front item", expectedOutput );
        Set_ActualOutput    ( "front item", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    FinishTestSet();
    return TestResult();
}

int Tester::Test_GetBack()
{
    StartTestSet( "Test_GetBack", { "" } );

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(2);
			test.GetBack();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "GetBack should throw exception if list is empty" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        bool threwException = false;

        try
        {
            string cd = ll.GetBack();
        }
        catch( ... )
        {
            threwException = true;
        }

        Set_ExpectedOutput  ( "Exception thrown" );
        if ( threwException )   Set_ActualOutput    ( "Exception thrown" );
        else                    Set_ActualOutput    ( "Exception not thrown" );

        if ( threwException == false )   { TestFail(); }
        else                             { TestPass(); }
    } FinishTest();


    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "After inserting several items, GetBack should get the back." ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "A" );
        ll.PushBack( "B" );
        ll.PushBack( "C" );

        string expectedOutput = "C";
        string actualOutput = ll.GetBack();

        Set_ExpectedOutput  ( "back data", expectedOutput );
        Set_ActualOutput    ( "back data", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_SubscriptOperator()
{
    StartTestSet( "Test_SubscriptOperator", { "PushBack" } );

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(123);
			int a = test[0];
			a = a;
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "operator[] should throw exception if list is empty" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        bool threwException = false;

        try
        {
            string cd = ll[10];
        }
        catch( ... )
        {
            threwException = true;
        }

        Set_ExpectedOutput  ( "Exception thrown" );
        if ( threwException )   Set_ActualOutput    ( "Exception thrown" );
        else                    Set_ActualOutput    ( "Exception not thrown" );

        if ( threwException == false )   { TestFail(); }
        else                             { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "After inserting several items, operator[] get the correct item." ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        ll.PushBack( "one" );
        ll.PushBack( "two" );
        ll.PushBack( "three" );

        string expectedOutput = "two";
        string actualOutput = ll[1];

        Set_ExpectedOutput  ( "#1 data", expectedOutput );
        Set_ActualOutput    ( "#1 data", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

int Tester::Test_IsEmpty()
{
    StartTestSet( "Test_IsEmpty", { "PushBack" } );

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(2);
			test.IsEmpty();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "An empty list should return true" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        bool expectedOutput = true;
        bool actualOutput = ll.IsEmpty();

        Set_ExpectedOutput  ( "IsEmpty()", expectedOutput );
        Set_ActualOutput    ( "IsEmpty()", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "A non-empty list should return true" ); {
        Set_Comments( "" );

        LinkedList<string> ll;
        ll.PushBack( "a" );
        ll.PushBack( "b" );
        ll.PushBack( "c" );

        bool expectedOutput = false;
        bool actualOutput = ll.IsEmpty();

        Set_ExpectedOutput  ( "IsEmpty()", expectedOutput );
        Set_ActualOutput    ( "IsEmpty()", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    FinishTestSet();
    return TestResult();
}

int Tester::Test_Size()
{
    StartTestSet( "Test_Size", { "PushBack" } );

	bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
		StartTest("Check if function has been implemented");
		try
		{
			LinkedList<int> test;
			test.PushBack(2);
			test.Size();
		}
		catch (...)
		{
			exceptOccur = true;
			TestFail();
		}

		if (!exceptOccur) { TestPass(); }
	} /* TEST END **************************************************************/

	if (exceptOccur)
	{
		FinishTestSet();
		return TestResult();
	}

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "The size of an empty list should be 0" ); {
        Set_Comments( "" );

        LinkedList<string> ll;

        int expectedOutput = 0;
        int actualOutput = ll.Size();

        Set_ExpectedOutput  ( "Size()", expectedOutput );
        Set_ActualOutput    ( "Size()", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "The size of a list with 3 items should be 3" ); {
        Set_Comments( "" );

        LinkedList<string> ll;
		ll.PushBack("hi1");
		ll.PushBack("hi2");
		ll.PushBack("hi3");

        int expectedOutput = 3;
        int actualOutput = ll.Size();

        Set_ExpectedOutput  ( "Size()", expectedOutput );
        Set_ActualOutput    ( "Size()", actualOutput );

        if ( expectedOutput != actualOutput )   { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest();


    FinishTestSet();
    return TestResult();
}

#endif
