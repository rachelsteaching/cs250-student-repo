#ifndef STACK_HPP
#define STACK_HPP

#include "LinkedList.hpp"

template <typename T>
class Stack
{
    public:
    //! Push a new item into the back of the stack
    void Push(const T& newData );
    //! Remove the item at the top of the stack
    void Pop() noexcept;
    //! Access the data at the top of the stack
    T& Top();
    //! Get the amount of items in the stack
    int Size();
    //! Return whether the stack is empty
    bool IsEmpty();

    private:
    LinkedList<T> m_list;
};

template <typename T>
void Stack<T>::Push(const T& newData )
{
    Logger::Out( "Function Begin", "Stack::Push" );
    cout << "Stack<T>::Push - NOT IMPLEMENTED" << endl;
}

template <typename T>
void Stack<T>::Pop() noexcept
{
    Logger::Out( "Function Begin", "Stack::Pop" );
    cout << "Stack<T>::Pop - NOT IMPLEMENTED" << endl;
}

template <typename T>
T& Stack<T>::Top()
{
    Logger::Out( "Function Begin", "Stack::Top" );
    cout << "Stack<T>::Top - NOT IMPLEMENTED" << endl;
}

template <typename T>
int Stack<T>::Size()
{
    Logger::Out( "Function Begin", "Stack::Size" );

    return m_list.Size();
}

template <typename T>
bool Stack<T>::IsEmpty()
{
    Logger::Out( "Function Begin", "Stack::IsEmpty" );

    return m_list.IsEmpty();
}

#endif
