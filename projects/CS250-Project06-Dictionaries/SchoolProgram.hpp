#ifndef SCHOOL_PROGRAM_HPP
#define SCHOOL_PROGRAM_HPP

#include "DATASTRUCTURES/Dictionary.hpp"
#include "cuTEST/Menu.hpp"
#include "cuTEST/StringUtil.hpp"
#include "School.hpp"
#include "Student.hpp"

class SchoolProgram
{
    public:
    void Start();

    private:
    Dictionary<int, School> m_schoolDict;
    Dictionary<int, Student> m_studentDict;
};

void SchoolProgram::Start()
{
    Logger::Out( "Function begin", "SchoolProgram::Start" );

    Menu::ClearScreen();
    Menu::Header( "School Program" );

    cout << " Select the collision method:" << endl;
    int collisionMethod = Menu::ShowIntMenuWithPrompt( { "Linear Probing", "Quadratic Probing", "Double Hashing" } );

    string filenameStudents = "out_students_";
    string filenameSchools = "out_schools_";

    if ( collisionMethod == 1 )         {
        m_schoolDict.SetCollisionMethod( LINEAR );
        m_studentDict.SetCollisionMethod( LINEAR );
        filenameStudents += "linear";
        filenameSchools += "linear";
    }
    else if ( collisionMethod == 2 )    {
        m_schoolDict.SetCollisionMethod( QUADRATIC );
        m_studentDict.SetCollisionMethod( QUADRATIC );
        filenameStudents += "quadratic";
        filenameSchools += "quadratic";
    }
    else                                {
        m_schoolDict.SetCollisionMethod( DOUBLE );
        m_studentDict.SetCollisionMethod( DOUBLE );
        filenameStudents += "double";
        filenameSchools += "double";
    }

    filenameStudents += ".csv";
    filenameSchools += ".csv";

    ifstream input( "actions.txt" );
    string buffer;

    while ( input >> buffer )
    {
        if ( buffer == "ADD_SCHOOL" )
        {
            string schoolName;
            int schoolId;

            input.ignore();
            getline( input, schoolName );
            input >> buffer;
            input >> schoolId;

            cout << " Adding SCHOOL ID " << schoolId << ": " << schoolName << endl;
            Logger::Out( "Add school " + schoolName, "SchoolProgram::Start" );
            m_schoolDict.Insert( schoolId, School( schoolName ) );
            Logger::Out( "Total schools: " + StringUtil::ToString( m_schoolDict.Size() ), "SchoolProgram::Start" );
        }
        else if ( buffer == "ADD_STUDENT" )
        {
            string studentName1, studentName2;
            int studentId;

            input >> studentName1 >> studentName2;
            input >> buffer;
            input >> studentId;

            cout << " Adding STUDENT ID " << studentId << ": " << studentName1 << " " << studentName2 << endl;
            Logger::Out( "Add student " + studentName1 + " " + studentName2, "SchoolProgram::Start" );
            m_studentDict.Insert( studentId, Student( studentName1, studentName2 ) );
            Logger::Out( "Total students: " + StringUtil::ToString( m_studentDict.Size() ), "SchoolProgram::Start" );
        }
        else if ( buffer == "PUT_STUDENT" )
        {
            int studentId;
            int schoolId;

            Student* student = nullptr;
            School* school = nullptr;

            input >> studentId;
            input >> buffer;
            input >> schoolId;

            try
            {
                student = m_studentDict.Get( studentId );
                student->schoolId = schoolId;
            }
            catch( runtime_error& ex )
            {
                cout << "Couldn't find student \"" << studentId << "\"" << endl;
            }

            try
            {
                school = m_schoolDict.Get( schoolId );
                student->schoolName = school->name;
            }
            catch( runtime_error& ex )
            {
                cout << "Couldn't find school \"" << schoolId << "\"" << endl;
            }

            cout << " Add STUDENT ID " << studentId << " to SCHOOL ID " << schoolId << endl;
            if ( student != nullptr && school != nullptr )
                Logger::Out( "Link student " + student->fname + " " + student->lname + " to school " + school->name, "SchoolProgram::Start" );
        }
    }

    cout << endl << endl << " Writing out dictionary structures to " << filenameStudents << " and " << filenameSchools << "..." << endl << endl;
    m_studentDict.WriteToFile( filenameStudents );
    m_schoolDict.WriteToFile( filenameSchools );

    Logger::Out( "Function end", "SchoolProgram::Start" );
}

#endif
