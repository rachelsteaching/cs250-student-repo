# Project 6: Dictionaries

## Viewing the documentation

Click on the **Classes** tab to view files, classes, and their documentation.
You will be implementing the Dictionary.

-----

## About

This project is a "School Database" that uses two dictionaries - one for
the Student objects. and one for the School objects.

```
Dictionary<int, School> m_schoolDict;
Dictionary<int, Student> m_studentDict;
```

The SchoolProgram is already implemented but you will be implementing the Dictionary data structure.

-----

## Class structure

![Diagram of the structures](../../dictionary-diagram.png)

The Dictionary contains a dynamic array of DictionaryNode objects.
A **key** is hashed into an **index** corresponding to the DictionaryNode index in the array.

The DictionaryNode is a templated class, containing a templated Key and templated Value.
The key will correspond to the Key passed in when inserting a new item. The value
will be either a School item or a Student item.

-----

## Input file

**Input files belong in the Project folder (CodeBlocks Project or VisualStudio Project)**

This file requires the ```actions.txt``` file to be present to load in a list of commands.
There are three types of commands in the actions file, which are already parsed by the
SchoolProgram class...

* Adding a school:

```
ADD_SCHOOL
Emerson Elementary
WITH_SCHOOL_ID
106
```

* Adding a student:

```
ADD_STUDENT
Bamber Malinde
WITH_STUDENT_ID
1288
```

* Linking a student ID to a school ID:

```
PUT_STUDENT
9720
IN SCHOOL
45
```

-----

## Output files

**Output files belong in the Project folder (CodeBlocks Project or VisualStudio Project)**

* ```test_result.html``` - the result of the unit tests

![Screenshot of test results](testexample.png)

* ```log.html``` - the debug log of the program. Any place that has a ```Logger::Out``` function call will write something in here. Use it to debug program flow.

![Screenshot of logger](loggerpreview.png)

* CSV files of the dictionary states for each collision method:
    * ```out_schools_linear.csv``` and ```out_students_linear.csv``` for Linear Probing
    * ```out_schools_quadratic.csv``` and ```out_students_quadratic.csv``` for Quadratic Probing
    * ```out_schools_double.csv``` and ```out_students_double.csv``` for Double Hashing

**Preview of out_students_linear.csv:**

![Screenshot of student file](studentpreview.png)

**Preview of out_schools_linear.csv:**

![Screenshot of school file](schoolpreview.png)

-----

## DictionaryNode information

### TK key

Unique key of the data being stored in the dictionary.

### TV value

Value of the data being stored in a cell in the dictionary.

### bool used

Marked ```false``` if this node is not in use (or was deleted), and ```true``` if something is stored here.

## Dictionary information

See also: the Dictionary documentation page.

### DictionaryNode<TK,TV>* m_vector

This is the dynamic array that is the structure of the dictionary.
The user only thinks about **KEYS** and **VALUES**, and we turn
those **KEYS** into **INDEX** values from 0 to size-1.

### CollisionMethod m_collisionMethod

This can be set to ```LINEAR```, ```QUADRATIC```, OR ```DOUBLE```.
This dictates to the ```FindNodeWithKey``` and ```FindUnusedNode```
which collision method to use.

### int m_arraySize

The size of the ```m_vector``` dynamic array.

### int m_itemCount

The amount of items currently stored in the ```m_vector```

### Dictionary()

Initializes the internal ```m_vector``` dynamic array to a prime number size.
Initializes ```m_itemCount``` to 0, and sets ```m_arraySize```.
Also defaults the ```m_collisionMethod``` to ```LINEAR```.

### ~Dictionary()

If memory is allocated to the ```m_vector```, then it frees that memory.

### void SetCollisionMethod( CollisionMethod cm )

Updates ```m_collisionMethod``` with whatever is passed in as ```cm```.


### int FindUnusedNode( TK key )

### int FindNodeWithKey( TK key )

**EASY MODE: Linear search (inefficient)**

**CHALLENGE MODE: Using Hashing / Collision resolution to find index based on key** (extra credit)




### void Insert( TK key, TV value )

Calls the ```FindUnusedNode``` function to generate an ```index``` based on the ```key```.
Stores the ```key``` and ```value``` at the ```m_vector[index]``` given the index
generated. Also must set the node's ```.used``` member to ```true```
and increment ```m_itemCount``` by 1.



### bool Exists( TK key )

Checks to see whether any of the elements of ```m_vector``` have a ```key```
whose value matches the parameter ```key```. Returns true if so, or false if none are found.

Call ```FindNodeWithKey```. If the index returned is valid (> 0, < m_arraySize-1), then
return ```true```. Otherwise, if it returns ```-1``` (invalid), return ```false```.


### TV* Get( TK key )

Calls the ```FindNodeWithKey``` function to find a node in ```m_vector```
whose ```.key``` member matches the ```key``` parameter passed in.

Call ```FindNodeWithKey``` to find the index of the node to retrieve.

### void Remove( TK key )

Removes an item from ```m_vector``` whose ```key``` matches the ```key``` parameter passed in.
Just set the ```.used``` member to ```false``` to "lazy delete" it.

Call ```FindNodeWithKey``` to find the index of the node to delete.





### int HashFunction( TK key )

Return ```key % m_arraySize```.


### int LinearProbe( TK originalIndex, int collisionCount )

Return ```originalIndex + collisionCount```.

### int QuadraticProbe( TK originalIndex, int collisionCount )

Return ```originalIndex + ( collisionCount * collisionCount )```.

### int HashFunction2( TK key )

Return ```7 - ( key % 7 )```.




### int Size()

Returns the amount of items currently stored in the dictionary, ```m_itemCount```

### void WriteToFile( const string& filename );

Writes out the state of the dictionary to a ```csv``` file.
