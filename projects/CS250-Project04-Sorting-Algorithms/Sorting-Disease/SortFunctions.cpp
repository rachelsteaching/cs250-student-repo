#include "SortFunctions.hpp"

void SelectionSort( vector<DataEntry>& data, const string& onKey )
{
    int n = data.size();

    for ( int j = 0; j < n-1; j++ )
    {
        int iMin = j;
        for ( int i = j+1; i < n; i++ )
        {
            if ( data[i].fields[ onKey ] < data[iMin].fields[ onKey ] )
            {
                iMin = i;
            }

            if ( iMin != j )
            {
                DataEntry temp = data[j];
                data[j] = data[iMin];
                data[iMin] = temp;
            }
        }
    }
}
