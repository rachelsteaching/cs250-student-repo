#ifndef _SELECTION_SORT_HPP
#define _SELECTION_SORT_HPP

//  Created by Frank M. Carrano and Tim Henry.
//  Copyright (c) 2016 __Pearson Education__. All rights reserved.

// Listing 11-1.

#include <iostream>
#include <string>
#include <fstream>

/** Finds the largest item in an array.
 @pre  The size of the array is >= 1.
 @post  The arguments are unchanged.
 @param theArray  The given array.
 @param size  The number of elements in theArray.
 @return  The index of the largest entry in the array. */
template<class ItemType>
int findIndexOfLargest(const ItemType theArray[], int size);

/** Sorts the items in an array into ascending order.
 @pre  None.
 @post  The array is sorted into ascending order; the size of the array
    is unchanged.
 @param theArray  The array to sort.
 @param n  The size of theArray. */
template<class ItemType>
void selectionSort(ItemType theArray[], int n)
{
    // last = index of the last item in the subarray of items yet
    //        to be sorted;
    // largest = index of the largest item found
    for (int last = n - 1; last >= 1; last--)
    {
        // At this point, theArray[last+1..n-1] is sorted, and its
        // entries are greater than those in theArray[0..last].
        // Select the largest entry in theArray[0..last]
        int largest = findIndexOfLargest(theArray, last+1);

        // Swap the largest entry, theArray[largest], with
        // theArray[last]
        std::swap(theArray[largest], theArray[last]);
    }  // end for
}  // end selectionSort

template<class ItemType>
int findIndexOfLargest(const ItemType theArray[], int size)
{
    int indexSoFar = 0; // Index of largest entry found so far
    for (int currentIndex = 1; currentIndex < size; currentIndex++)
    {
        // At this point, theArray[indexSoFar] >= all entries in
        // theArray[0..currentIndex - 1]
        if (theArray[currentIndex] > theArray[indexSoFar])
        {
            indexSoFar = currentIndex;
        }
    }  // end for
    return indexSoFar; // Index of largest entry
}  // end findIndexOfLargest

//int main()
//{
//   output.open( "selection-sort.txt" );
//   std::string a[3] = {"C", "A", "T"};
//   selectionSort(a, 3);
//   for (int i = 0; i < 3; i++)
//      std::cout << a[i] << " ";
//   std::cout << std::endl;
//   output.close();
//}  // end main

/*

B F K R X Z

*/

#endif
